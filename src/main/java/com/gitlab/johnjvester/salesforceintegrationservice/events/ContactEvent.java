package com.gitlab.johnjvester.salesforceintegrationservice.events;

import com.gitlab.johnjvester.salesforceintegrationservice.models.Contact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ContactEvent {
    private Contact contact;
}
